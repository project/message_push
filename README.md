# Message Push

The Message Push module is a light weight bridge between the message module and
the push framework. It fulfills a similar role to the message_notify module, but
with the full flexibility ot the push framework. It uses Flag module to allow
users to subscribe to changes to any sort of entity. In Message Push you define
subscription types that tie messages to flags and push types.

Some common use cases:

- Notify users when a comment is placed on their content
- Notify users when another user they follow posts a new blog post
- Notify users when a new event is created in their area

## What it doesn't do
The module does not create any messages itself. You will need some
other means of creating those, such as custom code, or the
[ECA module](https://drupal.org/project/eca). If you choose that path, also
consider tying the message to the push framework through ECA instead of this
module.

## Similar modules
- [Message Notify](https://drupal.org/project/message_notify)
- [ECA module](https://drupal.org/project/eca). This module is a bit more
  complex, but also more powerful. It allows you to create rules that can
  trigger messages.

## Requirements
This module needs [Message](https://drupal.org/project/message),
[Flag](https://drupal.org/project/flag), and
[Push Framework](https://drupal.org/project/push_framework). Some Push Framework
plugins are highly recommended.

## Installation
Install as you would normally install a contributed Drupal module.
See: https://www.drupal.org/node/895232 for further information.

## Configuration
- Enable the module
- Create a flag that will be used to subscribe to messages
- Create a message type that will be used to send messages
- Visit /admin/config/people/subscription-type
- Add a new subscription type tying the flag to the message type

## Maintainers

Current maintainers:

- [Eelke Blok (eelkeblok)](https://www.drupal.org/u/eelkeblok)
