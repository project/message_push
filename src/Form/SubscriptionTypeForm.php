<?php

declare(strict_types=1);

namespace Drupal\message_push\Form;

use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Component\Plugin\PluginManagerInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\message_push\Entity\SubscriptionType;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Subscription Type form.
 */
final class SubscriptionTypeForm extends EntityForm {

  /**
   * Drupal's entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected EntityFieldManagerInterface $entityFieldManager;

  /**
   * The channel plugin manager from the push framework module.
   *
   * @var \Drupal\Component\Plugin\PluginManagerInterface
   */
  protected PluginManagerInterface $channelPluginManager;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->setEntityTypeManager($container->get('entity_type.manager'));
    $instance->entityFieldManager = $container->get('entity_field.manager');
    $instance->channelPluginManager = $container->get('push_framework.channel.plugin.manager');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state): array {

    $form = parent::form($form, $form_state);

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $this->entity->label(),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $this->entity->id(),
      '#machine_name' => [
        'exists' => [SubscriptionType::class, 'load'],
      ],
      '#disabled' => !$this->entity->isNew(),
    ];

    $form['status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
      '#default_value' => $this->entity->status(),
    ];

    $form['description'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Description'),
      '#default_value' => $this->entity->get('description'),
    ];

    // Get message templates.
    $templateStorage = $this->entityTypeManager->getStorage('message_template');
    $templates = $templateStorage->loadMultiple();
    $options = [];

    foreach ($templates as $key => $template) {
      $options[$key] = $template->label();
    }

    $form['message_template'] = [
      '#type' => 'select',
      '#title' => $this->t('Message template'),
      '#options' => $options,
      '#description' => $this->t('The message template to subscribe to.'),
      '#default_value' => $this->entity->get('message_template'),
      '#ajax' => [
        'callback' => '::updateMessageField',
        'wrapper' => 'message-field-wrapper',
      ],
    ];

    $form['message_field'] = [
      '#type' => 'select',
      '#default_value' => 'none',
      '#title' => $this->t('Message field'),
      '#description' => $this->t('The reference field on the message template that should be checked for the flagged entity.'),
      '#prefix' => '<div id="message-field-wrapper">',
      '#suffix' => '</div>',
    ];

    $form['message_field'] = $this->updateMessageField($form, $form_state);

    // Push type. Select one of the available plugins or use the selected field
    // as a preference selector.
    $pushChannels = $this->channelPluginManager->getDefinitions();
    $options = [];

    foreach ($pushChannels as $key => $channel) {
      $options[$key] = $channel['label'];
    }
    $options['field'] = $this->t('Use field as preference selector');

    $form['push_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Push type'),
      '#options' => $options,
      '#description' => $this->t('The push channel to use, or allow the user to select the channel with the field selected below. The field should have possible values corresponding to a push type.'),
      '#default_value' => $this->entity->get('push_type'),
    ];

    // Get flags.
    try {
      // We take into consideration the Flag module is not available. We can
      // still do our thing, just without subscribing to individual entities.
      $flagStorage = $this->entityTypeManager->getStorage('flag');
      $flags = $flagStorage->loadMultiple();

      // @todo Make this more intelligent so that you can only select flags
      //   that could operate on entities from the selected message template?
      $options = ['none' => $this->t('None')];
      foreach ($flags as $key => $flag) {
        $options[$key] = $flag->label();
      }
      $form['flag'] = [
        '#type' => 'select',
        '#title' => $this->t('Flag'),
        '#options' => $options,
        '#description' => $this->t('A flag to allow users to subscribe to individual entities. The flag must be on an entity type that is referenced from the message template.'),
        '#default_value' => $this->entity->get('flag') ?? 'none',
      ];
    }
    catch (PluginNotFoundException $e) {
      // No problem, Flag does not exist.
    }

    // Get eligible fields for controlling preference.
    // @todo Extend with profile module fields.
    $fieldDefinitions = $this->entityFieldManager->getFieldDefinitions('user', 'user');
    $baseFields = $this->entityFieldManager->getBaseFieldDefinitions('user');

    $nonBaseFields = array_diff_key($fieldDefinitions, $baseFields);

    $form['preference_field'] = [
      '#type' => 'hidden',
      '#default_value' => 'none',
    ];

    // @todo Implement using fields to control preferences. The idea is to use
    //   a field on the user entity to use as a preference setting for receiving
    //   notifications. If the type of notification is set to 'field' above,
    //   possible values of the preference field should be valid push
    //   notification channel plugin identifiers. If not, it can be a simple
    //   checkbox or similar.
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state): int {
    $result = parent::save($form, $form_state);
    $message_args = ['%label' => $this->entity->label()];
    $this->messenger()->addStatus(
      match($result) {
        \SAVED_NEW => $this->t('Created new subscription type %label.', $message_args),
        \SAVED_UPDATED => $this->t('Updated subscription type %label.', $message_args),
      }
    );
    $form_state->setRedirectUrl($this->entity->toUrl('collection'));
    return $result;
  }

  /**
   * Update the message field based on the selected message template.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   The updated field.
   */
  public function updateMessageField(array $form, FormStateInterface $form_state) {
    // Get the value of the message_template field.
    $messageTemplate = $form_state->getValue('message_template');

    if (empty($messageTemplate)) {
      $messageTemplate = $this->entity->get('message_template');
    }

    // Based on the value of the message_template field, update the options of
    // the message_field field.
    $messageFieldOptions = empty($messageTemplate) ? [] : $this->getMessageFieldOptions($messageTemplate);

    if (empty($messageFieldOptions)) {
      $messageFieldOptions = ['none' => $this->t('None')];
      $form['message_field']['#disabled'] = TRUE;
    }
    $form['message_field']['#options'] = $messageFieldOptions;

    // Return the updated field.
    return $form['message_field'];
  }

  /**
   * Get options for the message field based on the selected message template.
   *
   * @param string $messageTemplate
   *   The selected message template.
   *
   * @return array
   *   The options for the message field.
   */
  private function getMessageFieldOptions(string $messageTemplate) {
    // Get all fields from the message template.
    $entityType = 'message';

    // Get all field definitions for the specified entity type and bundle.
    $fieldDefinitions = $this->entityFieldManager->getFieldDefinitions($entityType, $messageTemplate);

    // Find all entity reference fields in the field definitions.
    $entityReferenceFields = array_filter($fieldDefinitions, function (FieldDefinitionInterface $fieldDefinition) {
      return $fieldDefinition->getType() === 'entity_reference';
    });

    $options = [];

    foreach ($entityReferenceFields as $fieldName => $fieldDefinition) {
      $options[$fieldName] = $fieldDefinition->getLabel();
    }

    return $options;
  }

}
