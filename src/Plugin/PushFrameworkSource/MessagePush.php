<?php

namespace Drupal\message_push\Plugin\PushFrameworkSource;

use Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\State\StateInterface;
use Drupal\Core\Utility\Error;
use Drupal\push_framework\ChannelPluginInterface;
use Drupal\push_framework\SourceBase;
use Drupal\push_framework\SourceItem;
use Drupal\push_framework\SourcePluginInterface;
use Drupal\user\UserInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the push framework source.
 *
 * @SourcePlugin(
 *   id = "message_push",
 *   label = @Translation("Message Push"),
 *   description = @Translation("Provides all the notifications that need to be pushed.")
 * )
 */
class MessagePush extends SourceBase {

  /**
   * Drupal's state service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected StateInterface $state;

  /**
   * A logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected LoggerChannelInterface $logger;

  /**
   * Key for state service to store last message processed.
   */
  const STATE_KEY_LAST_MESSAGE_ID = 'message_push.last_message';

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition,
  ): static {
    $instance = parent::create(
      $container,
      $configuration,
      $plugin_id,
      $plugin_definition);
    $instance->logger = $container->get('logger.channel.message_push');
    $instance->state = $container->get('state');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function getAllItemsForPush(): array {
    // Find the messages for which subscriptions exist since the last time we
    // ran.
    $previousMessageId = $this->state->get(self::STATE_KEY_LAST_MESSAGE_ID);

    if (is_null($previousMessageId)) {
      // This must be the first time we run. We'll return now and start doing
      // our thing for all new messages.
      $this->setStateToVeryLast();
      return [];
    }

    // Get the subscription types.
    $subscriptionTypeStorage = $this->entityTypeManager->getStorage('subscription_type');
    $subscriptionTypeIds = $subscriptionTypeStorage->getQuery()
      ->accessCheck(FALSE)
      ->condition('status', TRUE)
      ->execute();
    /** @var \Drupal\message_push\Entity\SubscriptionType[] $subscriptionTypes */
    $subscriptionTypes = $subscriptionTypeStorage->loadMultiple($subscriptionTypeIds);

    if (empty($subscriptionTypes)) {
      // No subscription types, so nothing to do either.
      $this->setStateToVeryLast();
      return [];
    }

    // Find in which message templates we are interested.
    $subscriptionTypesByMessageTemplates = [];
    foreach ($subscriptionTypes as $subscriptionType) {
      $subscriptionTypesByMessageTemplates[$subscriptionType->get('message_template')][] = $subscriptionType;
    }

    /** @var \Drupal\Core\Entity\ContentEntityStorageInterface $messageStorage */
    $messageStorage = $this->entityTypeManager->getStorage('message');

    // We get a limited number of messages per run. We can't really tell how
    // many pushes that will result in. We should probably make this
    // configurable.
    $select = $messageStorage->getQuery()
      ->accessCheck(FALSE)
      ->condition('mid', $previousMessageId, '>')
      ->condition('template', array_keys($subscriptionTypesByMessageTemplates), 'IN')
      ->range(0, 25);
    $messageIds = $select->execute();

    if (empty($messageIds)) {
      // Nothing to do right now. Set the state to the very last ID. This
      // prevents the system accumulating a huge backlog for messages we don't
      // have subscriptions for now, but may be added later.
      $this->setStateToVeryLast();
      return [];
    }

    $flagStorage = $this->entityTypeManager->getStorage('flag');
    /** @var \Drupal\flag\Entity\Storage\FlaggingStorageInterface $flaggingStorage */
    $flaggingStorage = $this->entityTypeManager->getStorage('flagging');

    $messages = $messageStorage->loadMultiple($messageIds);

    // Loop over all the messages and find if we need to notify anyone about
    // them.
    $items = [];
    foreach ($messages as $message) {
      // Tick off this message.
      $this->state->set(self::STATE_KEY_LAST_MESSAGE_ID, $message->id());
      // Find which subscription types are relevant to this message.
      $relevantSubscriptionTypes = $subscriptionTypesByMessageTemplates[$message->bundle()];

      foreach ($relevantSubscriptionTypes as $subscriptionType) {
        // Find the entity referenced by the message that is relevant to the
        // flag for the subscription type.
        /** @var \Drupal\message_push\SubscriptionTypeInterface $subscriptionType */
        $flagId = $subscriptionType->get('flag');
        $flag = $flagStorage->load($flagId);

        if (empty($flag)) {
          // Log the exception, but do nothing else.
          $this->logger->error('Flag with ID %id not found.', ['%id' => $flagId]);
          continue;
        }

        // Get the subject entity from the message based on the message field
        // from the subscription.
        try {
          $subjectEntity = $message->get($subscriptionType->get('message_field'))->entity;
        }
        catch (\Exception $e) {
          // Log the exception, but do nothing else.
          Error::logException($this->logger, $e);
          continue;
        }

        // Find the flaggings for the flag on the subject.
        $flaggingIds = $flaggingStorage->getQuery()
          ->accessCheck(FALSE)
          ->condition('flag_id', $flag->id())
          ->condition('entity_id', $subjectEntity->id())
          ->execute();

        // For each flagging, create a source item of the user, unless it is the
        // user themselves (i.e. it is the author of the message).
        foreach ($flaggingIds as $flaggingId) {
          $flagging = $flaggingStorage->load($flaggingId);
          $user = $flagging->get('uid')->entity;
          if ($user->id() !== $message->getOwner()->id()) {
            $items[] = new SourceItem($this, $message->id(), $user->id());
          }
        }
      }
    }

    return $items;
  }

  /**
   * {@inheritdoc}
   */
  public function getObjectAsEntity(string $oid): ?ContentEntityInterface {
    /** @var \Drupal\Core\Entity\ContentEntityStorageInterface $messageStorage */
    $messageStorage = $this->entityTypeManager->getStorage('message');
    /** @var \Drupal\message\MessageInterface $message */
    $message = $messageStorage->load($oid);

    return $message ?? NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function confirmAttempt(
    string $oid,
    UserInterface $user,
    ChannelPluginInterface $channelPlugin,
    string $result,
  ): SourcePluginInterface {
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function confirmDelivery(
    string $oid,
    UserInterface $user,
  ): SourcePluginInterface {
    return $this;
  }

  /**
   * Set our state variable to the very last ID.
   *
   * In several situations, we want to set our state variable to the very last
   * message ID. It creates a sensible baseline for subsequent processing in
   * case there is nothing to do. If we took the last ID of a message we
   * actually have subscriptions for, we might build up a backlog of messages
   * when
   * - no new messages are created of types we are interested in;
   * - new messages are created for a type we add a subscription for later.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function setStateToVeryLast(): void {
    try {
      $messageStorage = $this->entityTypeManager->getStorage('message');
      $select = $messageStorage->getQuery()
        ->accessCheck(FALSE);

      // We find the last message ID overall. We could only store IDs for
      // message templates we are interested in, but that could cause weird
      // timing issues when we create a new subscription type, and it has been a
      // while since we've had to send a notification for other messages.
      $messageIds = $select->sort('mid', 'DESC')->range(0, 1)->execute();
      $lastMessageId = empty($messageIds) ? 0 : reset($messageIds);
      // Set the last message ID for a few early return conditions.
      $this->state->set(self::STATE_KEY_LAST_MESSAGE_ID, $lastMessageId);
    }
    catch (InvalidPluginDefinitionException | PluginNotFoundException $e) {
      // Log the exception, but do nothing else.
      Error::logException($this->logger, $e);
    }
  }

}
