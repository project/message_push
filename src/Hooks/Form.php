<?php

namespace Drupal\message_push\Hooks;

use Drupal\Core\Form\FormStateInterface;
use Drupal\push_framework\Form\Settings;

/**
 * Hook implementations for form-related hooks.
 */
class Form {

  /**
   * Implements hook_form_alter().
   */
  public function alter(&$form, FormStateInterface $form_state, $form_id) {
    // We are going to add message tokens to the token browser in the push
    // framework settings forms, since we're enabling the to work.
    $formObject = $form_state->getFormObject();

    if (!$formObject instanceof Settings) {
      return $form;
    }

    if (isset($form['content']['pattern']['token_help'])) {
      $form['content']['pattern']['token_help']['#token_types']['message'] = 'message';
    }

    return $form;
  }

}
