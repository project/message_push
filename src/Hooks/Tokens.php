<?php

namespace Drupal\message_push\Hooks;

use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Render\BubbleableMetadata;
use Drupal\Core\Utility\Token;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Token-related hook implementations.
 */
class Tokens implements ContainerInjectionInterface {

  /**
   * The token service.
   *
   * @var \Drupal\Core\Utility\Token
   */
  public Token $tokenService;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  public EntityTypeManagerInterface $entityTypeManager;

  /**
   * Constructs a new Tokens object.
   */
  public function __construct(
    EntityTypeManagerInterface $entityTypeManager,
    Token $tokenService,
  ) {
    $this->entityTypeManager = $entityTypeManager;
    $this->tokenService = $tokenService;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('token'),
    );
  }

  /**
   * Implements hook_tokens().
   */
  public function hookTokens($type, $tokens, array $data, array $options, BubbleableMetadata $bubbleableMetadata) {
    if ($type !== 'message' || !isset($data['push_framework_source_plugin']) || $data['push_framework_source_plugin']->getPluginId() !== 'message_push') {
      return [];
    }
    $messageStorage = $this->entityTypeManager->getStorage('message');
    $message = $messageStorage->load($data['push_framework_source_id']);

    if (!$message) {
      return [];
    }

    return $this->tokenService->generate(
      'message',
      $tokens,
      ['message' => $message],
      $options,
      $bubbleableMetadata
    );
  }

}
