<?php

declare(strict_types=1);

namespace Drupal\message_push\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\message_push\SubscriptionTypeInterface;

/**
 * Defines the subscription type entity type.
 *
 * @ConfigEntityType(
 *   id = "subscription_type",
 *   label = @Translation("Subscription Type"),
 *   label_collection = @Translation("Subscription Types"),
 *   label_singular = @Translation("subscription type"),
 *   label_plural = @Translation("subscription types"),
 *   label_count = @PluralTranslation(
 *     singular = "@count subscription type",
 *     plural = "@count subscription types",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\message_push\SubscriptionTypeListBuilder",
 *     "form" = {
 *       "add" = "Drupal\message_push\Form\SubscriptionTypeForm",
 *       "edit" = "Drupal\message_push\Form\SubscriptionTypeForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *     },
 *     "storage" = "Drupal\message_push\SubscriptionTypeStorage",
 *   },
 *   config_prefix = "subscription_type",
 *   admin_permission = "administer subscription_type",
 *   links = {
 *     "collection" = "/admin/config/people/subscription-type",
 *     "add-form" = "/admin/config/people/subscription-type/add",
 *     "edit-form" = "/admin/config/people/subscription-type/{subscription_type}",
 *     "delete-form" = "/admin/config/people/subscription-type/{subscription_type}/delete",
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "description",
 *     "message_template",
 *     "message_field",
 *     "flag",
 *     "push_type",
 *     "preference_field",
 *   },
 * )
 */
final class SubscriptionType extends ConfigEntityBase implements SubscriptionTypeInterface {

  /**
   * The example ID.
   *
   * @var string
   */
  protected string $id;

  /**
   * The example label.
   *
   * @var string
   */
  protected string $label;

  /**
   * The example description.
   *
   * @var string
   */
  protected string $description;

}
