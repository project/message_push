<?php

namespace Drupal\message_push;

use Drupal\Core\Config\Entity\ConfigEntityStorage;

/**
 * Storage handler for subscription type entities.
 *
 * Doesn't do anything as yet.
 */
class SubscriptionTypeStorage extends ConfigEntityStorage {

}
