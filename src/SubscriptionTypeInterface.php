<?php

declare(strict_types=1);

namespace Drupal\message_push;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining a subscription type entity type.
 */
interface SubscriptionTypeInterface extends ConfigEntityInterface {

}
