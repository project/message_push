<?php

namespace Drupal\Tests\message_push\Functional;

use Drupal\Core\State\StateInterface;
use Drupal\flag\FlagInterface;
use Drupal\flag\FlagServiceInterface;
use Drupal\message\Entity\Message;
use Drupal\message_push\Entity\SubscriptionType;
use Drupal\message_push\Plugin\PushFrameworkSource\MessagePush;
use Drupal\push_framework\SourceItem;
use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\field\Traits\EntityReferenceTestTrait;
use Drupal\Tests\flag\Traits\FlagCreateTrait;
use Drupal\Tests\message\Kernel\MessageTemplateCreateTrait;
use Drupal\user\UserInterface;

/**
 * Tests the MessagePush source plugin.
 *
 * @group message_push
 */
class SubscribedMessageTest extends BrowserTestBase {

  // When we drop support for Drupal 10.1, we need to change this to
  // CreateEntityReferenceFieldTrait.
  // @phpstan-ignore-next-line
  use EntityReferenceTestTrait;
  use FlagCreateTrait;
  use MessageTemplateCreateTrait;

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'field',
    'flag',
    'message_push',
    'message',
    'node',
    'push_framework',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Push Framework is missing a configuration schema.
   *
   * @var bool
   */
  protected $strictConfigSchema = FALSE;

  /**
   * The flag service.
   *
   * @var \Drupal\flag\FlagServiceInterface
   */
  protected FlagServiceInterface $flagService;

  /**
   * The state service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected StateInterface $state;

  /**
   * A flag to use to subscribe to nodes.
   *
   * @var \Drupal\flag\FlagInterface
   */
  protected FlagInterface $flag;

  /**
   * The user subscribing to the message.
   *
   * @var \Drupal\user\UserInterface
   */
  protected UserInterface $subscriberUser;

  /**
   * The user creating the message.
   *
   * @var \Drupal\user\UserInterface
   */
  protected UserInterface $creatorUser;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->flagService = $this->container->get('flag');
    $this->state = $this->container->get('state');
    $this->drupalCreateContentType([
      'type' => 'post',
      'name' => 'Post',
      'display_submitted' => FALSE,
    ]);

    // Create message template.
    $this->createMessageTemplate('example_template');
    $this->createEntityReferenceField('message', 'example_template', 'field_node', 'Node', 'node');

    // Create a flag to use as a subscription flag.
    $this->flag = $this->createFlag('node', ['post']);

    // Set the state variable such that we won't skip the messages we'll create.
    $this->state->set(MessagePush::STATE_KEY_LAST_MESSAGE_ID, 0);

    // Create a subscription type.
    $subscriptionType = SubscriptionType::create(
      [
        'id' => 'subscribe_post',
        'label' => 'Subscribe to posts',
        'description' => 'Subscribe to posts',
        'message_template' => 'example_template',
        'message_field' => 'field_node',
        'flag' => $this->flag->id(),
        'status' => TRUE,
      ]
    );
    $subscriptionType->save();

    $this->subscriberUser = $this->drupalCreateUser();
    $this->creatorUser = $this->drupalCreateUser();
  }

  /**
   * Test subscribing to messages.
   */
  public function testSubscribing() {
    // Create a node.
    $node = $this->drupalCreateNode([
      'type' => 'post',
    ]);

    // Create flagging on the node.
    $this->flagService->flag($this->flag, $node, $this->subscriberUser);

    // Create a message.
    $message = Message::create([
      'template' => 'example_template',
      'uid' => $this->creatorUser->id(),
      'field_node' => $node->id(),
    ]);
    $message->save();

    // Get the MessagePush plugin.
    $messagePushPlugin = \Drupal::service('push_framework.source.plugin.manager')->createInstance('message_push');

    // Generate the source items.
    $sourceItems = $messagePushPlugin->getAllItemsForPush();

    // Check that a source item was generated for the message.
    $this->assertCount(1, $sourceItems, 'One source item was generated.');

    // Check that the source item has the correct properties.
    $sourceItem = reset($sourceItems);
    $this->assertInstanceOf(SourceItem::class, $sourceItem, 'The source item is an instance of SourceItem.');
    $info = $sourceItem->toArray();
    $this->assertEquals($node->id(), $info['oid'], 'The source item has the correct object ID.');
    $this->assertEquals($this->subscriberUser->id(), $sourceItem->getUid(), 'The source item has the correct user ID.');
  }

  /**
   * Test if there is no push when the user is not subscribed.
   */
  public function testNoPushWhenNotSubscribed() {
    // Create a node.
    $node = $this->drupalCreateNode([
      'type' => 'post',
    ]);

    // Create a message. We did not flag the node, so the user is not
    // subscribed.
    $message = Message::create([
      'template' => 'example_template',
      'uid' => $this->creatorUser->id(),
      'field_node' => $node->id(),
    ]);
    $message->save();

    // Get the MessagePush plugin.
    $messagePushPlugin = \Drupal::service('push_framework.source.plugin.manager')->createInstance('message_push');

    // Generate the source items.
    $sourceItems = $messagePushPlugin->getAllItemsForPush();

    // Check that a source item was generated for the message.
    $this->assertCount(0, $sourceItems, 'No source items were generated.');
  }

  /**
   * Test if there is no push for the user's own messages.
   */
  public function testNoPushWhenOwnMessage() {
    // Create a node.
    $node = $this->drupalCreateNode([
      'type' => 'post',
    ]);

    // Create flagging on the node.
    $this->flagService->flag($this->flag, $node, $this->subscriberUser);

    // Create a message. We did not flag the node, so the user is not
    // subscribed.
    $message = Message::create([
      'template' => 'example_template',
      'uid' => $this->subscriberUser->id(),
      'field_node' => $node->id(),
    ]);
    $message->save();

    // Get the MessagePush plugin.
    $messagePushPlugin = \Drupal::service('push_framework.source.plugin.manager')->createInstance('message_push');

    // Generate the source items.
    $sourceItems = $messagePushPlugin->getAllItemsForPush();

    // Check that a source item was generated for the message.
    $this->assertCount(0, $sourceItems, 'No source items were generated.');
  }

}
